package com.example.footballbnn.data

import androidx.annotation.Keep

@Keep
data class FootballBnnSplashResponse(val url : String)