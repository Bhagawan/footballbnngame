package com.example.footballbnn.util.navigation

import android.webkit.WebView
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.footballbnn.data.objects.AppData
import com.example.footballbnn.ui.screens.CountrySelectScreen
import com.example.footballbnn.ui.screens.NetworkErrorScreen
import com.example.footballbnn.ui.screens.Screens
import com.example.footballbnn.ui.screens.SplashScreen
import com.example.footballbnn.ui.screens.WebViewScreen
import com.example.footballbnn.ui.screens.gameScreen.GameScreen
import com.example.footballbnn.ui.screens.menuScreen.MenuScreen
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: WebView, fixateScreen:  () -> Unit, retryInternetAccess: () -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            BackHandler(true) { exitProcess(0) }
            SplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.canGoBack()) webView.goBack()
                else exitProcess(0)
            }
            WebViewScreen(webView, remember { AppData.url })
        }
        composable( Screens.MENU_SCREEN.label ) {
            BackHandler(true) { Navigator.navigateTo(Screens.COUNTRY_SELECTION_SCREEN) }
            MenuScreen()
        }
        composable( Screens.GAME_SCREEN.label ) {
            BackHandler(true) { Navigator.navigateTo(Screens.MENU_SCREEN) }
            GameScreen()
        }
        composable( Screens.COUNTRY_SELECTION_SCREEN.label ) {
            BackHandler(true) { exitProcess(0) }
            CountrySelectScreen()
        }
        composable( Screens.NETWORK_ERROR_SCREEN.label ) {
            BackHandler(true) { exitProcess(0) }
            NetworkErrorScreen(retryInternetAccess)
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen == Screens.MENU_SCREEN) fixateScreen()
    navController.navigate(currentScreen.label) {
        launchSingleTop = true
    }
    navController.enableOnBackPressed(true)
}