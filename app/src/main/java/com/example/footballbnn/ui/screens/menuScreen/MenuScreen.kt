package com.example.footballbnn.ui.screens.menuScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.footballbnn.R
import com.example.footballbnn.data.objects.UrlBack
import com.example.footballbnn.data.objects.UrlBase
import com.example.footballbnn.data.objects.UrlLogo
import com.example.footballbnn.ui.composables.PLayButton
import com.example.footballbnn.ui.screens.Screens
import com.example.footballbnn.util.navigation.Navigator

@Preview
@Composable
fun MenuScreen() {
    val viewModel = viewModel(MenuScreenViewModel::class.java)
    val botCountryUrl by viewModel.botCountryFlagUrl.collectAsState()
    val playerCountryUrl by viewModel.playerCountryFlagUrl.collectAsState()
    val botClub by viewModel.botClub.collectAsState()
    val playerClub by viewModel.playerClub.collectAsState()

    Image(rememberAsyncImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(10.dp), verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        Image(rememberAsyncImagePainter(UrlLogo), contentDescription = null, modifier = Modifier.size(75.dp), contentScale = ContentScale.FillBounds)
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(text = stringResource(id = R.string.player),
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.weight(1.0f, true))
            Text(text = stringResource(id = R.string.bot),
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.weight(1.0f, true))
        }
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                Image(rememberAsyncImagePainter(playerCountryUrl),
                    contentDescription = null,
                    contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .size(50.dp)
                        .background(Color.Transparent, CircleShape)
                        .clipToBounds()
                        .clip(CircleShape)
                )
            }
            Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
                Image(rememberAsyncImagePainter(botCountryUrl),
                    contentDescription = null,
                    contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .size(50.dp)
                        .background(Color.Transparent, CircleShape)
                        .clipToBounds()
                        .clip(CircleShape)
                )
            }
        }
        Row(modifier = Modifier
            .fillMaxWidth()
            .weight(3.0f), verticalAlignment = Alignment.CenterVertically) {
            BoxWithConstraints( modifier = Modifier
                .weight(1.0f, true)
                .padding(10.dp), contentAlignment = Alignment.Center) {
                Image(rememberAsyncImagePainter(UrlBase + playerClub.logoUrl),
                    contentDescription = null,
                    contentScale = ContentScale.Fit
                )
            }
            BoxWithConstraints( modifier = Modifier
                .weight(1.0f, true)
                .padding(10.dp), contentAlignment = Alignment.Center) {
                Image(rememberAsyncImagePainter(UrlBase + botClub.logoUrl),
                    contentDescription = null,
                    contentScale = ContentScale.Fit
                )
            }
        }
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(text = playerClub.name,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.weight(1.0f, true))
            Text(text = botClub.name,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier.weight(1.0f, true))
        }
        Box(modifier = Modifier.weight(1.0f, true), contentAlignment = Alignment.Center) {
            Text(text = stringResource(id = R.string.btn_random),
                fontSize = 30.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White,
                modifier = Modifier
                    .clickable(remember { MutableInteractionSource() }, null) {
                        viewModel.randomise()
                    })
        }
        BoxWithConstraints(modifier = Modifier
            .weight(1.0f, true)
            .fillMaxWidth(), contentAlignment = Alignment.Center) {
            val w = maxWidth
            PLayButton(modifier = Modifier.width(w * 0.5f)) {
                Navigator.navigateTo(Screens.GAME_SCREEN)
            }
        }
    }
}