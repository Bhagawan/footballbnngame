package com.example.footballbnn.data.objects

import com.example.footballbnn.data.Club
import com.example.footballbnn.data.Country

object AppData {
    var url = ""
    var countries = emptyList<Country>()

    var selectedPlayerCountryFlagUrl: String = ""
    var selectedPlayerClub: Club = Club("","")
    var selectedBotCountryFlagUrl: String = ""
    var selectedBotClub: Club = Club("","")
}