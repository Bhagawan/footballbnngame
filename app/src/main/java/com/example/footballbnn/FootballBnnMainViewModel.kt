package com.example.footballbnn

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.footballbnn.data.objects.AppData
import com.example.footballbnn.ui.screens.Screens
import com.example.footballbnn.util.api.FootballBnnServerClient
import com.example.footballbnn.util.navigation.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

class FootballBnnMainViewModel: ViewModel() {
    private var request: Job? = null
    private val server = FootballBnnServerClient.create()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryInternetAccess() {
        viewModelScope.launch {
            switchToApp()
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToApp()
                            }
                            else -> viewModelScope.launch {
                                AppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToApp()
                } else switchToApp()
            }
        } catch (e: Exception) {
            switchToApp()
        }
    }

    private suspend fun switchToApp() {
        val countries = server.getCountries()
        if(countries.isSuccessful) {
            if(countries.body() != null) {
                AppData.countries = countries.body()!!
                Navigator.navigateTo(Screens.COUNTRY_SELECTION_SCREEN)
            } else internetError()
        } else internetError()
    }

    private fun internetError() {
        Navigator.navigateTo(Screens.NETWORK_ERROR_SCREEN)
    }
}