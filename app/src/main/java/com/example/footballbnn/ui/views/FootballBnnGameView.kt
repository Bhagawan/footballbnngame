package com.example.footballbnn.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.footballbnn.R
import com.example.footballbnn.data.Vector2D
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Float.max
import java.lang.Float.min
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.math.asin
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt
import kotlin.random.Random

class FootballBnnGameView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var speed = 1.0f

    private val ballBitmap = ResourcesCompat.getDrawable(context.resources, R.drawable.football, null)?.toBitmap(200, 200) ?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
    private var botBitmap: Bitmap? = null
    private var playerBitmap: Bitmap? = null

    private var playerRadius = 200.0f
    private var ballRadius = 200.0f

    private var botScore = 0
    private var playerScore = 0

    private val playerPosition = Vector2D(0.0f,0.0f)
    private val botPosition = Vector2D(0.0f,0.0f)
    private val ballPosition = Vector2D(0.0f, 0.0f)

    private val ballMoveVector = Vector2D(0.0f,0.0f)
    private val botMoveVector = Vector2D()
    private val playerMoveVector = Vector2D()

    private var mInterface: GameInterface? = null

    private var state = STATE_GAME

    companion object {
        const val STATE_PAUSE = 0
        const val STATE_GAME = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            playerRadius = mWidth / 20.0f
            ballRadius = playerRadius
            speed = mWidth / 120.0f
            restartGame()
        }
    }

    override fun onDraw(canvas: Canvas) {
        if(state == STATE_GAME) {
            updateBot()
            updateBallPosition()
            clearPlayerMovement()
            checkGoal()
        }

        drawObjects(canvas)
        drawScore(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                movePlayerTo(event.x, event.y)
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                movePlayerTo(event.x, event.y)
                return true
            }

            MotionEvent.ACTION_UP -> {
                playerMoveVector.set(0.0f, 0.0f)
                return true
            }
        }
        return false
    }

    //// Public

    fun setPlayerBitmap(pBitmap: Bitmap) {
        playerBitmap = pBitmap
    }

    fun setBotBitmap(bBitmap: Bitmap) {
        botBitmap = bBitmap
    }

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun resetGame() {
        state = STATE_GAME
        ballPosition.set(mWidth / 2.0f, mHeight / 2.0f)
        playerPosition.set(mWidth / 2.0f, mHeight - playerRadius)
        botPosition.set(mWidth / 2.0f, playerRadius)

        ballMoveVector.set(Random.nextFloat() * speed, Random.nextFloat() * speed)
    }

    fun restartGame() {
        playerScore = 0
        botScore = 0
        resetGame()
    }

    //// Private

    private fun drawObjects(c: Canvas) {
        val p = Paint()
        p.strokeWidth = 5.0f
        if(playerBitmap == null) {
            p.color = Color.BLUE
            c.drawCircle(playerPosition.x, playerPosition.y, playerRadius, p)
        } else c.drawBitmap(playerBitmap!!, null,
            Rect((playerPosition.x - playerRadius).toInt(),
                (playerPosition.y - playerRadius).toInt(),
                (playerPosition.x + playerRadius).toInt(),
                (playerPosition.y + playerRadius).toInt()), p)

        if(botBitmap == null) {
            p.color = Color.RED
            c.drawCircle(botPosition.x, botPosition.y, playerRadius, p)
        } else c.drawBitmap(botBitmap!!, null,
            Rect((botPosition.x - playerRadius).toInt(),
                (botPosition.y - playerRadius).toInt(),
                (botPosition.x + playerRadius).toInt(),
                (botPosition.y + playerRadius).toInt()), p)

        c.drawBitmap(ballBitmap, null,
            Rect((ballPosition.x - ballRadius).toInt(),
                (ballPosition.y - ballRadius).toInt(),
                (ballPosition.x + ballRadius).toInt(),
                (ballPosition.y + ballRadius).toInt()), p)
    }

    private fun drawScore(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.textAlign = Paint.Align.LEFT
        p.textSize = 100.0f
        p.isFakeBoldText = true

        c.drawText(botScore.toString(), mWidth * 0.9f, mHeight * 0.1f, p)
        c.drawText(playerScore.toString(), mWidth * 0.9f, mHeight * 0.9f, p)
    }

    private fun updateBallPosition() {
        var collided = false
        val future = Vector2D(ballPosition.x + ballMoveVector.x, ballPosition.y + ballMoveVector.y)

        val distToBot = future.distTo(botPosition)
        var distToFutureLine = (ballMoveVector.y * botPosition.x - ballMoveVector.x * botPosition.y + future.x * ballPosition.y - future.y * ballPosition.x).absoluteValue / ballMoveVector.len()
        if(distToBot <= ballRadius + playerRadius) {
            val halfHord = sqrt((playerRadius + ballRadius).pow(2) - distToFutureLine.pow(2))
            val distBallBot = ballPosition.distTo(botPosition)

            val half = sqrt(distBallBot.pow(2) - distToFutureLine.pow(2))
            val movOrt = ballMoveVector.ort()
            val mid = Vector2D(ballPosition.x + movOrt.x * half, ballPosition.y + movOrt.y * half)

            val p1 = Vector2D(mid.x + movOrt.x * halfHord, mid.y + movOrt.y * halfHord)
            val p2 = Vector2D(mid.x - movOrt.x * halfHord, mid.y - movOrt.y * halfHord)

            val collP = if(ballPosition.distTo(p1) <= ballPosition.distTo(p2)) p1 else p2
            if(ballPosition.distTo(collP) <= ballMoveVector.len() + playerRadius + ballRadius) {
                collided = true
                ballPosition.set(collP.x)
                val startA = asin(distToFutureLine / (ballRadius + playerRadius))
                val a = Math.PI - startA * 2
                val newX = ballMoveVector.x * cos(a) - ballMoveVector.y * sin(a) + botMoveVector.x
                val newY = ballMoveVector.x * sin(a) + ballMoveVector.y * cos(a) + botMoveVector.y
                ballMoveVector.set(newX.toFloat(), newY.toFloat())
            }
        }

        if(!collided) {
            val distToPlayer = future.distTo(playerPosition)
            distToFutureLine = (ballMoveVector.y * playerPosition.x - ballMoveVector.x * playerPosition.y + future.x * ballPosition.y - future.y * ballPosition.x).absoluteValue / ballMoveVector.len()
            if(distToPlayer <= ballRadius + playerRadius) {
                val halfHord = sqrt((playerRadius + ballRadius).pow(2) - distToFutureLine.pow(2))
                val distBallPlayer = ballPosition.distTo(playerPosition)

                val half = sqrt(distBallPlayer.pow(2) - distToFutureLine.pow(2))
                val movOrt = ballMoveVector.ort()
                val mid = Vector2D(ballPosition.x + movOrt.x * half, ballPosition.y + movOrt.y * half)

                val p1 = Vector2D(mid.x + movOrt.x * halfHord, mid.y + movOrt.y * halfHord)
                val p2 = Vector2D(mid.x - movOrt.x * halfHord, mid.y - movOrt.y * halfHord)

                val collP = if(ballPosition.distTo(p1) <= ballPosition.distTo(p2)) p1 else p2
                if(ballPosition.distTo(collP) <= ballMoveVector.len() + playerRadius + ballRadius) {
                    ballPosition.set(collP.x)
                    val startA = asin(distToFutureLine / (ballRadius + playerRadius))
                    val a = Math.PI - startA * 2
                    val newX = ballMoveVector.x * cos(a) - ballMoveVector.y * sin(a) + playerMoveVector.x
                    val newY = ballMoveVector.x * sin(a) + ballMoveVector.y * cos(a) + playerMoveVector.y
                    ballMoveVector.set(newX.toFloat(), newY.toFloat())
                }
            }
        }

        if(ballPosition.x + ballMoveVector.x <= ballRadius) {
            ballPosition.x = ballRadius
            ballMoveVector.x *= -1
            val l = (ballPosition.x - ballRadius).absoluteValue / ballMoveVector.x
            ballPosition.y += ballMoveVector.y * l
        } else if(ballPosition.x + ballMoveVector.x >= mWidth - ballRadius) {
            ballPosition.x = mWidth - ballRadius
            ballMoveVector.x *= -1
            val l = (ballPosition.x - (mWidth -  ballRadius)).absoluteValue / ballMoveVector.x
            ballPosition.y += ballMoveVector.y * l
        } else {
            ballPosition.x += ballMoveVector.x
            ballPosition.y += ballMoveVector.y
        }
    }

    private fun movePlayerTo(x: Float, y: Float) {
        val newX = x.coerceAtLeast(playerRadius).coerceAtMost(mWidth - playerRadius)
        val newY = y.coerceAtLeast(mHeight / 2 + playerRadius + ballRadius).coerceAtMost(mHeight - playerRadius)
        playerMoveVector.set(newX - playerPosition.x, newY - playerPosition.y)

        val dX = playerMoveVector.x / -10.0f
        val dY = playerMoveVector.y / -10.0f
        val bD = Vector2D()
        while(Vector2D(playerPosition.x + playerMoveVector.x, playerPosition.y + playerMoveVector.y).distTo(ballPosition) < (ballRadius + playerRadius) * 0.8f) {
            bD.x += dX
            bD.y += dY
            playerMoveVector.x += dX
            playerMoveVector.y += dY
        }
        ballPosition.x += -bD.x
        ballPosition.y += -bD.y

        playerPosition.set(playerPosition.x + playerMoveVector.x, playerPosition.y + playerMoveVector.y)
    }

    private fun clearPlayerMovement() {
        playerMoveVector.set(0.0f, 0.0f)
    }

    private fun updateBot() {
        botMoveVector.x = (ballPosition.x - botPosition.x).coerceAtMost(min(speed, mWidth - playerRadius - botPosition.x)).coerceAtLeast(max(-speed, playerRadius -botPosition.x))

        botMoveVector.y = if(ballMoveVector.y > 0) (playerRadius - botPosition.y).coerceAtLeast(-speed)
        else (ballPosition.y - botPosition.y).coerceAtMost(min(speed, mHeight / 2 - playerRadius - ballRadius - botPosition.y)).coerceAtLeast(max(-speed, playerRadius - botPosition.y))

        botPosition.x += botMoveVector.x
        botPosition.y += botMoveVector.y
    }

    private fun checkGoal() {
        if(ballPosition.y < -ballRadius) {
            state = STATE_PAUSE
            if(++playerScore >= 10 ) mInterface?.endGame(botScore, playerScore) else mInterface?.goal()
        } else if(ballPosition.y > mHeight + ballRadius) {
            state = STATE_PAUSE
            if(++botScore >= 10 ) mInterface?.endGame(botScore, playerScore) else mInterface?.goal()
        }
    }

    interface GameInterface {
        fun goal()
        fun endGame(botScore: Int, playerScore: Int)
    }
}