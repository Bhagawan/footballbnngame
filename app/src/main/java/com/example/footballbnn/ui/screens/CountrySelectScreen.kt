package com.example.footballbnn.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.example.footballbnn.data.objects.AppData
import com.example.footballbnn.data.objects.UrlBack
import com.example.footballbnn.data.objects.UrlBase
import com.example.footballbnn.data.objects.UrlLogo
import com.example.footballbnn.ui.composables.RoundFlag
import com.example.footballbnn.util.navigation.Navigator

@Composable
fun CountrySelectScreen() {
    val state = rememberLazyGridState()
    Image(rememberAsyncImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
    Column(modifier = Modifier.fillMaxSize().padding(10.dp), verticalArrangement = Arrangement.spacedBy(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        LazyVerticalGrid(columns = GridCells.Fixed(2),
            state = state,
            contentPadding = PaddingValues(10.dp),
            verticalArrangement = Arrangement.spacedBy(5.dp),
            horizontalArrangement = Arrangement.spacedBy(5.dp),
            modifier = Modifier.weight(2.0f).fillMaxWidth()
        ) {
            items(AppData.countries) { country ->
                RoundFlag(imageUrl = UrlBase + country.flagUrl, modifier = Modifier.fillMaxWidth().aspectRatio(1.0f).padding(15.dp)) {
                    AppData.selectedPlayerCountryFlagUrl = UrlBase + country.flagUrl
                    AppData.selectedPlayerClub = country.clubs.random()

                    val botC = AppData.countries.random()
                    var botClub = botC.clubs.random()
                    while(botClub == AppData.selectedPlayerClub) botClub = botC.clubs.random()
                    AppData.selectedBotCountryFlagUrl = UrlBase + botC.flagUrl
                    AppData.selectedBotClub = botClub
                    Navigator.navigateTo(Screens.MENU_SCREEN)
                }
            }
        }
        Image(rememberAsyncImagePainter(UrlLogo), contentDescription = null, modifier = Modifier.size(100.dp), contentScale = ContentScale.FillBounds)
    }
}