package com.example.footballbnn.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.footballbnn.R
import com.example.footballbnn.ui.theme.Blue
import com.example.footballbnn.ui.theme.Green
import com.example.footballbnn.ui.theme.Grey
import com.example.footballbnn.ui.theme.Red
import com.example.footballbnn.ui.theme.Transparent_black

@Composable
fun WinPopup(botScore: Int, playerScore: Int, onRestart: () -> Unit, onExit: () -> Unit) {
    val string = stringResource(id = R.string.win,
        if(botScore > playerScore) stringResource(id = R.string.bot) else stringResource(id = R.string.player)
    )
    val scoreString = if(botScore > playerScore) "$botScore - $playerScore" else "$playerScore - $botScore"
    val color = if(botScore > playerScore) Red else Blue

    Box(modifier = Modifier
        .fillMaxSize()
        .background(Transparent_black), contentAlignment = Alignment.Center) {
        AnimatedVisibility(visible = true,
            enter = fadeIn(tween(200)),
            exit = fadeOut(tween(100))
        ) {
            Column(modifier = Modifier
                .fillMaxWidth(0.8f)
                .background(Transparent_black, RoundedCornerShape(10.dp))
                .padding(10.dp),
                verticalArrangement = Arrangement.spacedBy(10.dp),
                horizontalAlignment = Alignment.CenterHorizontally) {
                Text(string, fontWeight = FontWeight.Bold, fontSize = 30.sp, textAlign = TextAlign.Center, color = color)
                Text(scoreString, fontWeight = FontWeight.Bold, fontSize = 20.sp, textAlign = TextAlign.Center, color = Color.White)
                Row(modifier = Modifier.fillMaxWidth(0.8f), verticalAlignment = Alignment.CenterVertically, horizontalArrangement = Arrangement.SpaceEvenly) {
                    IconButton(onClick = { onRestart() },
                        colors = IconButtonDefaults.iconButtonColors(
                            containerColor = Grey,
                            contentColor = Color.Magenta
                        ),
                        modifier = Modifier
                            .background(Grey, RoundedCornerShape(5.dp))
                            .padding(5.dp)) {
                        Icon(painterResource(id = R.drawable.baseline_restart), contentDescription = null, tint = Green, modifier = Modifier.size(100.dp))
                    }

                    IconButton(onClick = { onExit() },
                        colors = IconButtonDefaults.iconButtonColors(
                            containerColor = Grey,
                            contentColor = Red
                        ),
                        modifier = Modifier
                            .background(Grey, RoundedCornerShape(5.dp))
                            .padding(5.dp)) {
                        Icon(painterResource(id = R.drawable.baseline_arrow_back_24), contentDescription = null, tint = Red, modifier = Modifier.size(100.dp))
                    }
                }
            }
        }
    }
}