package com.example.footballbnn.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Country(val flagUrl: String,
    @SerializedName("clubsUrl") val clubs: List<Club>)
