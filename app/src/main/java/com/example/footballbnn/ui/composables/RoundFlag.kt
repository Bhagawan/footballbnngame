package com.example.footballbnn.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import coil.compose.rememberAsyncImagePainter

@Composable
fun RoundFlag(modifier: Modifier = Modifier, imageUrl: String, onPress: () -> Unit) {
    Box(modifier = modifier
        .background(Color.Transparent, CircleShape)
        .clipToBounds()
        .clickable(remember { MutableInteractionSource() }, null) {
            onPress()
        }, contentAlignment = Alignment.Center) {
        Image(rememberAsyncImagePainter(imageUrl),
            contentDescription = null,
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.fillMaxSize().clip(CircleShape)
        )
    }
}