package com.example.footballbnn.ui.screens.menuScreen

import androidx.lifecycle.ViewModel
import com.example.footballbnn.data.objects.AppData
import com.example.footballbnn.data.objects.UrlBase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class MenuScreenViewModel: ViewModel() {
    private val _playerCountryFlagUrl = MutableStateFlow(AppData.selectedPlayerCountryFlagUrl)
    val playerCountryFlagUrl = _playerCountryFlagUrl.asStateFlow()

    private val _botCountryFlagUrl = MutableStateFlow(AppData.selectedBotCountryFlagUrl)
    val botCountryFlagUrl = _botCountryFlagUrl.asStateFlow()

    private val _playerClub = MutableStateFlow(AppData.selectedPlayerClub)
    val playerClub = _playerClub.asStateFlow()

    private val _botClub = MutableStateFlow(AppData.selectedBotClub)
    val botClub = _botClub.asStateFlow()

    fun randomise() {
        val pCountry = AppData.countries.random()
        val pClub = pCountry.clubs.random()
        val bCountry = AppData.countries.random()
        var bClub = bCountry.clubs.random()
        while(bClub == pClub) bClub = bCountry.clubs.random()

        AppData.selectedPlayerCountryFlagUrl = UrlBase + pCountry.flagUrl
        AppData.selectedPlayerClub = pClub
        AppData.selectedBotCountryFlagUrl = UrlBase + bCountry.flagUrl
        AppData.selectedBotClub = bClub

        _playerCountryFlagUrl.tryEmit(UrlBase + pCountry.flagUrl)
        _playerClub.tryEmit(pClub)
        _botCountryFlagUrl.tryEmit(UrlBase + bCountry.flagUrl)
        _botClub.tryEmit(bClub)
    }
}