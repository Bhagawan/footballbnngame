package com.example.footballbnn.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Club(@SerializedName("name") val  name: String, @SerializedName("logoUrl") val logoUrl: String)
