package com.example.footballbnn.util.api

import com.example.footballbnn.data.Country
import com.example.footballbnn.data.FootballBnnSplashResponse
import com.example.footballbnn.data.objects.UrlCountries
import com.example.footballbnn.data.objects.UrlSplash
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface FootballBnnServerClient {

    @FormUrlEncoded
    @POST(UrlSplash)
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<FootballBnnSplashResponse>

    @GET(UrlCountries)
    suspend fun getCountries(): Response<List<Country>>

    companion object {
        fun create() : FootballBnnServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(FootballBnnServerClient::class.java)
        }
    }
}