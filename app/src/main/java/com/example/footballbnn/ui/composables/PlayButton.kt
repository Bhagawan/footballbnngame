package com.example.footballbnn.ui.composables

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.footballbnn.R

@Composable
fun PLayButton(modifier: Modifier = Modifier, onClick: () -> Unit) {
    Box(modifier = modifier
        .border(width = 5.dp, color = Color.White, shape = RoundedCornerShape(10.dp))
        .clipToBounds()
        .clickable {
            onClick()
        }, contentAlignment = Alignment.Center) {
            Text(text = stringResource(id = R.string.btn_play),
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                color = Color.White)
    }
}