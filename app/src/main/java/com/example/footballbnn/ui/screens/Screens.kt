package com.example.footballbnn.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    COUNTRY_SELECTION_SCREEN("country_select"),
    MENU_SCREEN("menu"),
    NETWORK_ERROR_SCREEN("network_error"),
    GAME_SCREEN("game")
}