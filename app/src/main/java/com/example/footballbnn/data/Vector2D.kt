package com.example.footballbnn.data

import kotlin.math.pow
import kotlin.math.sqrt

class Vector2D(var x: Float = 0.0f, var y: Float = 0.0f) {


    fun set(newX: Float = x, newY: Float = y) {
        x = newX
        y = newY
    }

    fun set(newV: Vector2D) {
        x = newV.x
        y = newV.y
    }

    fun len(): Float = sqrt(x.pow(2) + y.pow(2))

    fun ort(): Vector2D  {
        val l = len()
        return Vector2D(x / l, y / l)
    }

    fun distTo(point: Vector2D) = sqrt((point.x - x).pow(2) + (point.y - y).pow(2))
}
