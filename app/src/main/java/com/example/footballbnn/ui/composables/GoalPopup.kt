package com.example.footballbnn.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.example.footballbnn.data.objects.UrlGoal

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun GoalPopup() {
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        AnimatedVisibility(visible = true,
            enter = scaleIn(spring(stiffness = Spring.StiffnessMedium)),
            exit = fadeOut(tween(300))
        ) {
            Image(rememberAsyncImagePainter(UrlGoal), contentDescription = null, contentScale = ContentScale.FillBounds, modifier = Modifier.fillMaxWidth(0.8f).height(100.dp))
        }
    }
}