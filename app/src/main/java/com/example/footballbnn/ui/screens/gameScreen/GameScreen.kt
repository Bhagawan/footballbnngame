package com.example.footballbnn.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.footballbnn.data.objects.UrlField
import com.example.footballbnn.ui.composables.GoalPopup
import com.example.footballbnn.ui.composables.WinPopup
import com.example.footballbnn.ui.views.FootballBnnGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun GameScreen() {
    var init  by rememberSaveable { mutableStateOf(true) }
    val viewModel = viewModel(GameScreenViewModel::class.java)
    if(init) { viewModel.load(LocalContext.current) }
    val goalPopup by viewModel.goalPopup.collectAsState()
    val winPopup by viewModel.winPopup.collectAsState()

    Image(rememberAsyncImagePainter(UrlField), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    AndroidView(factory = { FootballBnnGameView(it) },
        modifier = Modifier.fillMaxSize(),
        update = {view ->
            if (init) {
                viewModel.resetGame.onEach { view.resetGame() }.launchIn(viewModel.viewModelScope)
                viewModel.restartGame.onEach { view.restartGame() }.launchIn(viewModel.viewModelScope)
                viewModel.botBitmap.onEach { it?.let { view.setBotBitmap(it) } }.launchIn(viewModel.viewModelScope)
                viewModel.playerBitmap.onEach { it?.let { view.setPlayerBitmap(it) } }.launchIn(viewModel.viewModelScope)

                view.setInterface(object: FootballBnnGameView.GameInterface {
                    override fun goal() {
                        viewModel.goal()
                    }

                    override fun endGame(botScore: Int, playerScore: Int) {
                        viewModel.endGame(botScore, playerScore)
                    }
                })
                init = false
            }
        })

    if(goalPopup) GoalPopup()
    winPopup?.let {
        WinPopup(botScore = it.first, playerScore = it.second, onRestart = { viewModel.restart() }, onExit = { viewModel.exit() })
    }
}