package com.example.footballbnn.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Transparent_black = Color(0x80000000)
val Blue = Color(0xFF006B9C)
val Red = Color(0x80FF0000)
val Grey = Color(0x801B1B1B)
val Green = Color(0x80257E15)