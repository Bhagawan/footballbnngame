package com.example.footballbnn.ui.screens.gameScreen

import android.content.Context
import android.graphics.Bitmap
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import coil.ImageLoader
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.example.footballbnn.data.objects.AppData
import com.example.footballbnn.data.objects.UrlBase
import com.example.footballbnn.ui.screens.Screens
import com.example.footballbnn.util.navigation.Navigator
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameScreenViewModel: ViewModel() {
    private val _botBitmap = MutableStateFlow<Bitmap?>(null)
    val botBitmap = _botBitmap.asStateFlow()

    private val _playerBitmap = MutableStateFlow<Bitmap?>(null)
    val playerBitmap = _playerBitmap.asStateFlow()

    private val _winPopup = MutableStateFlow<Pair<Int, Int>?>(null)
    val winPopup = _winPopup.asStateFlow()

    private val _goalPopup = MutableStateFlow(false)
    val goalPopup = _goalPopup.asStateFlow()

    private val _resetGame = MutableSharedFlow<Boolean>()
    val resetGame = _resetGame.asSharedFlow()

    private val _restartGame = MutableSharedFlow<Boolean>()
    val restartGame = _restartGame.asSharedFlow()

    fun load(context: Context) {
        val loader = ImageLoader(context)
        val botRequest = ImageRequest.Builder(context)
            .data(UrlBase + AppData.selectedBotClub.logoUrl)
            .size(200,200)
            .allowHardware(false)
            .listener(object: ImageRequest.Listener {
                override fun onSuccess(request: ImageRequest, result: SuccessResult) {
                    viewModelScope.launch {
                        _botBitmap.emit(result.drawable.toBitmap())
                    }
                }
            })
            .build()
        val playerRequest = ImageRequest.Builder(context)
            .data(UrlBase + AppData.selectedPlayerClub.logoUrl)
            .size(200,200)
            .allowHardware(false)
            .listener(object: ImageRequest.Listener {
                override fun onSuccess(request: ImageRequest, result: SuccessResult) {
                    viewModelScope.launch {
                        _playerBitmap.emit(result.drawable.toBitmap())
                    }
                }
            })
            .build()
        viewModelScope.launch {
            loader.execute(botRequest)
            loader.execute(playerRequest)
        }
    }

    fun goal() {
        viewModelScope.launch {
            _goalPopup.emit(true)
            delay(1000)
            _goalPopup.emit(false)
            _resetGame.emit(true)
        }
    }

    fun endGame(botScore: Int, playerScore: Int) {
        _winPopup.tryEmit(botScore to playerScore)
    }

    fun restart() {
        viewModelScope.launch {
            _winPopup.tryEmit(null)
            _restartGame.emit(true)
        }
    }

    fun exit() {
        Navigator.navigateTo(Screens.COUNTRY_SELECTION_SCREEN)
    }

}