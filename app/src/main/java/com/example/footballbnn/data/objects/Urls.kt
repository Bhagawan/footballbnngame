package com.example.footballbnn.data.objects

const val UrlSplash = "FootballBnnGame/splash.php"
const val UrlLogo = "http://195.201.125.8/FootballBnnGame/logo.png"
const val UrlBack = "http://195.201.125.8/FootballBnnGame/back.jpg"
const val UrlCountries = "http://195.201.125.8/FootballBnnGame/countries.json"
const val UrlField = "http://195.201.125.8/FootballBnnGame/assets/field.jpg"
const val UrlBase = "http://195.201.125.8/FootballBnnGame/"
const val UrlGoal = "http://195.201.125.8/FootballBnnGame/assets/goal.png"